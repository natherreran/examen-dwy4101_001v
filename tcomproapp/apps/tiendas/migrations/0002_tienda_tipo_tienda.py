# Generated by Django 2.0.9 on 2018-11-14 05:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tiendas', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tienda',
            name='tipo_tienda',
            field=models.CharField(choices=[('Online', 'Online'), ('Fisica', 'Fisica')], default='', max_length=20),
            preserve_default=False,
        ),
    ]
