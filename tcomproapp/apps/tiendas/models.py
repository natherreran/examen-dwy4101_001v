from django.db import models

# Create your models here.

class Tienda(models.Model):
    tipo_tienda = (
        ('Online', 'Online'),
        ('Fisica', 'Fisica')
    )
    nombre = models.CharField(max_length=50, unique=True)
    sucursal = models.CharField(max_length=50)
    tipo_tienda = models.CharField(max_length=20,choices=tipo_tienda)
    direccion = models.CharField(max_length=50)
    ciudad = models.CharField(max_length=50)
    region = models.CharField(max_length=50)
    foto = models.ImageField(default='default.png', blank=True)
    disponible = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.nombre)
