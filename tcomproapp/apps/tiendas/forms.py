from django import forms

from apps.tiendas.models import Tienda

class RegistroTiendaForm(forms.ModelForm):
    
    class Meta:
        model = Tienda

        fields = [
            'nombre',
            'sucursal',
            'tipo_tienda',
            'direccion',
            'ciudad',
            'region',
            'foto'
        ]
        labels = {
            'nombre' : 'Nombre Tienda',
            'sucursal' : 'Nombre Sucursal',
            'tipo_tienda' : 'Tipo Tienda',
            'direccion' : 'Direccion',
            'ciudad' : 'Ciudad',
            'region' : 'Región',
            'foto' : 'Foto'
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'sucursal' : forms.TextInput(attrs={'class':'form-control'}),
            'tipo_tienda' : forms.Select(attrs={'class':'form-control'}),
            'direccion' : forms.TextInput(attrs={'class':'form-control'}),
            'ciudad' : forms.TextInput(attrs={'class':'form-control'}),
            'region' : forms.TextInput(attrs={'class':'form-control'}),
        }

class ModificarTienda(forms.ModelForm):
    class Meta:
        model = Tienda
        fields = [
            'nombre',
            'sucursal',
            'tipo_tienda',
            'direccion',
            'ciudad',
            'region',
            'foto'
        ]
        labels = {
            'nombre' : 'Nombre Tienda',
            'sucursal' : 'Nombre Sucursal',
            'tipo_tienda' : 'Tipo Tienda',
            'direccion' : 'Direccion',
            'ciudad' : 'Ciudad',
            'region' : 'Región',
            'foto' : 'Foto'
            
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'sucursal' : forms.TextInput(attrs={'class':'form-control'}),
            'tipo_tienda' : forms.Select(attrs={'class':'form-control'}),
            'direccion' : forms.TextInput(attrs={'class':'form-control'}),
            'ciudad' : forms.TextInput(attrs={'class':'form-control'}),
            'region' : forms.TextInput(attrs={'class':'form-control'}),
        }