from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404

from apps.tiendas.forms import RegistroTiendaForm
from apps.tiendas.forms import ModificarTienda

from apps.tiendas.models import Tienda


# Create your views here.

@login_required()
def listado(request):
    if request.user.is_superuser:
        tienda = Tienda.objects.all() 
    else:
        tienda = Tienda.objects.filter(disponible = 1) 

    contexto = {'tiendas' : tienda}
    return render(request, 'tiendas/listado.html', contexto)


@login_required()
def crear(request):
    if request.method == 'POST':
        form = RegistroTiendaForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
        return redirect('tiendas:listado')
    else:
        form = RegistroTiendaForm()
    return render(request, 'tiendas/registro.html', {'form':form})

@login_required()
def editar(request, id=None):
    instance = get_object_or_404(Tienda, id=id)
    form = ModificarTienda(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('tiendas:listado')
    context = {
        "id":instance.id,
        "instance":instance,
        "form":form,
    }
    return render(request, 'tiendas/editar.html', context)

@login_required()
def eliminar(request, id=None):
    instance = get_object_or_404(Tienda, id=id)
    instance.delete()
    return redirect('tiendas:listado')
