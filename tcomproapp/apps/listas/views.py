from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404

from . import forms

from apps.listas.models import Lista

# Create your views here.

@login_required()
def crear(request):
    if request.method == 'POST':
        form = forms.RegistroListaForm(request.POST,request.FILES)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.usuario_id = request.user.id
            form.save()
            messages.success(request, 'Se ha registrado lista exitosamente')
        return redirect('listas:listado')
    else:
        form = forms.RegistroListaForm()
    return render(request, 'listas/registro.html', {'form':form})

@login_required()
def listado(request):
    lista = Lista.objects.filter(usuario=request.user.id)
    contexto = {'listas' : lista}
    return render(request, 'listas/listado.html', contexto)

@login_required()
def editar(request, id=None):
    instance = get_object_or_404(Lista, id=id)
    form = forms.ModificarLista(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('listas:listado')
    context = {
        "id":instance.id,
        "instance":instance,
        "form":form,
    }
    return render(request, 'listas/editar.html', context)

@login_required()
def eliminar(request, id=None):
    instance = get_object_or_404(Lista, id=id)
    instance.delete()
    return redirect('listas:listado')

@login_required()
def lista(request, id=None):
    instance = get_object_or_404(Lista, id=id)
    contexto = {'lista' : instance}
    return render(request, 'listas/lista.html', contexto)
