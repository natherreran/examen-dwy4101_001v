from django import forms

from apps.listas.models import Lista

class RegistroListaForm(forms.ModelForm):
    class Meta:
        model = Lista

        fields = [
            'nombre',
        ]
        labels = {
            'nombre' : 'Nombre de la Lista',
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
        }

class ModificarLista(forms.ModelForm):
    class Meta:
        model = Lista
        fields = [
            'nombre',
        ]
        labels = {
            'nombre' : 'Nombre Lista',            
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
        }