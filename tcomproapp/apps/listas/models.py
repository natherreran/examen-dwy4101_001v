from django.db import models

# Create your models here. 

class Lista(models.Model):
    nombre = models.CharField(max_length=50)
    usuario = models.ForeignKey('auth.User', on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.nombre)




