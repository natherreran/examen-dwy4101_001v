from django.conf.urls import include, url

from apps.listas import views

app_name = 'listas'

urlpatterns = [
    url(r'^$', views.listado, name='listado'),
    url(r'^nuevo$', views.crear, name='crear'),
    url(r'^(?P<id>\d+)/$', views.lista, name='lista'),
    url(r'^editar/(?P<id>\d+)/$', views.editar, name='editar'),
    url(r'^eliminar/(?P<id>\d+)/$', views.eliminar, name='eliminar'),
]
