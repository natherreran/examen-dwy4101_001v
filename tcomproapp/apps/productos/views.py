from django.shortcuts import render, redirect
from django.http import HttpResponse

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404

from apps.productos.forms import RegistroProductoForm
from apps.productos.forms import ModificarProducto
from apps.productos.models import Producto
# Create your views here.

@login_required()
def listado(request):
    producto = Producto.objects.all() 
    contexto = {'productos' : producto}
    return render(request, 'productos/listado.html', contexto)

@login_required()
def crear(request):
    if request.method == 'POST':
        form = RegistroProductoForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
        return redirect('productos:crear')
    else:
        form = RegistroProductoForm()
    return render(request, 'productos/registro.html', {'form':form})

@login_required()
def editar(request, id=None):
    instance = get_object_or_404(Producto, id=id)
    form = ModificarProducto(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return redirect('productos:listado')
    context = {
        "id":instance.id,
        "instance":instance,
        "form":form,
    }
    return render(request, 'productos/editar.html', context)

@login_required()
def eliminar(request, id=None):
    instance = get_object_or_404(Producto, id=id)
    instance.delete()
    return redirect('productos:listado')
