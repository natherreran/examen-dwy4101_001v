from django.conf.urls import include, url
from apps.productos import views

app_name = 'productos'

urlpatterns = [
    url(r'^$', views.listado, name='listado'),
    url(r'^nuevo$', views.crear, name='crear'),
    url(r'^editar/(?P<id>\d+)/$', views.editar, name='editar'),
    url(r'^eliminar/(?P<id>\d+)/$', views.eliminar, name='eliminar'),
]