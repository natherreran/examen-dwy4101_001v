from django.db import models
from apps.listas.models import Lista
from apps.tiendas.models import Tienda

# Create your models here.

class Producto(models.Model):
    nombre = models.CharField(max_length=50)
    costo_presupuestado = models.IntegerField()
    costo_real = models.IntegerField()
    tienda = models.ForeignKey(Tienda, on_delete=models.CASCADE)
    notas_adicionales = models.CharField(max_length=300)
    lista = models.ForeignKey(Lista, on_delete=models.CASCADE)
    comprado = models.BooleanField(default=False)

    def __str__(self):
        return '{}'.format(self.nombre)



    

