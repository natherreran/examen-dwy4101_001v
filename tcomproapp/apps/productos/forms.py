from django import forms

from apps.productos.models import Producto

class RegistroProductoForm(forms.ModelForm):
    
    class Meta:
        model = Producto

        fields = [
            'nombre',
            'costo_presupuestado',
            'costo_real',
            'tienda',
            'notas_adicionales'
        ]
        labels = {
            'nombre' : 'Nombre producto',
            'costo_presupuestado' : 'Costo Presupuestado',
            'costo_real' : 'Costo Real',
            'tienda' : 'Tienda',
            'notas_adicionales' : 'Notas Adicionales' 
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'costo_presupuestado' : forms.TextInput(attrs={'class':'form-control'}),
            'costo_real' : forms.TextInput(attrs={'class':'form-control'}),
            'tienda' : forms.Select(attrs={'class':'form-control'}),
            'notas_adicionales' : forms.TextInput(attrs={'class':'form-control'})
        }

class ModificarProducto(forms.ModelForm):
    
    class Meta:
        model = Producto

        fields = [
            'nombre',
            'costo_presupuestado',
            'costo_real',
            'tienda',
            'notas_adicionales'
        ]
        labels = {
            'nombre' : 'Nombre producto',
            'costo_presupuestado' : 'Costo Presupuestado',
            'costo_real' : 'Costo Real',
            'tienda' : 'Tienda',
            'notas_adicionales' : 'Notas Adicionales' 
        }
        widgets = {
            'nombre' : forms.TextInput(attrs={'class':'form-control'}),
            'costo_presupuestado' : forms.TextInput(attrs={'class':'form-control'}),
            'costo_real' : forms.TextInput(attrs={'class':'form-control'}),
            'tienda' : forms.Select(attrs={'class':'form-control'}),
            'notas_adicionales' : forms.TextInput(attrs={'class':'form-control'})
        }