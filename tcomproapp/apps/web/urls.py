from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'web'

urlpatterns = [
    path(r'', views.home, name='home'),
    path(r'dashboard', views.dashboard, name='dashboard'),
]