from tastypie.resources import ModelResource, ALL_WITH_RELATIONS, fields
from tastypie.authorization import Authorization

from apps.listas.models import Lista
from apps.productos.models import Producto
from apps.tiendas.models import Tienda

class TiendaResource(ModelResource):
    class Meta:
        queryset = Tienda.objects.all()
        resource_name = 'tiendas'
        authorization = Authorization()
        filtering = {
            "id": ('exact'),
            "disponible": ('exact')
        }

class ListaResource(ModelResource):
    class Meta:
        queryset = Lista.objects.all()
        resource_name = 'listas'
        authorization = Authorization()
        filtering = {
            "id": ('exact',)
        }

class ProductoResource(ModelResource):
    lista = fields.ForeignKey(ListaResource, 'lista', full=True)
    tienda = fields.ForeignKey(TiendaResource, 'tienda', full=True)
    class Meta:
        queryset = Producto.objects.all()
        resource_name = 'productos'
        authorization = Authorization()
        filtering = {
            'lista': ALL_WITH_RELATIONS,
        }

