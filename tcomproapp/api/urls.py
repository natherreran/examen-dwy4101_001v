from django.conf.urls import include, url
from django.contrib import admin
from tastypie.api import Api
from api.resources import ListaResource, ProductoResource, TiendaResource

v1_api = Api(api_name='v1')
v1_api.register(ListaResource())
v1_api.register(ProductoResource())
v1_api.register(TiendaResource())

app_name = 'api'

urlpatterns = [
    url(r'^', include(v1_api.urls)),
]
