/*----------------------------inicio--------------------------------

[Script - principal ]

Proyecto:  TCompro App
Version:  v1.0
Ultimo cambio: 27-11-18  -  11:00 SEBC
Asignado a:  ----
Primary use:  App. 

----------------------

[Tabla de contenido]

1.Inicializacion de controles.
2.Controles generales.
3.Controles de contacto.

** Recomendaciones para navegacion de tabla de contenido **

[Shortcuts]

1.Ctrl+inicio (regresas a la linea 1 del archivo).
2.Ctrl+fin    (te lleva a la ultima linea del archivo).

-------------------------fin---------------------------------*/
$(function () {
    init();
});

// 1.Inicializacion de controles.
function init() {
    confiGenerales.init();

    if ($(".dashboard").length) {
        dashboard.init();
    }

    if ($(".contacto").length) {
        contacto.init();
    }

    if ($(".lista-interna").length) {
        lista.init();
    }

    if ($(".tiendas").length) {
        tiendas.init();
    }
}
// 2.Controles generales.
var confiGenerales = {
    init: function () {
        confiGenerales.copyrigth();
        confiGenerales.oldBrowsers();
    },
    copyrigth: function () {
        $(".js-year").html((new Date).getFullYear());
    },
    oldBrowsers: function () {
        var promiseSupport = false;
        try {
            var promise = new Promise(function (x, y) {});
            promiseSupport = true;
        } catch (e) {
            var head = document.getElementsByTagName('head')[0];
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js';
            head.appendChild(script);
        }
        if (window.ActiveXObject || "ActiveXObject" in window) {
            alert("Ferouch estÃ¡ optimizado para versiones superiores a IE11, es posible que no pueda disfrutar de todas las funcionalidades que ofrecemos con esta versiÃ³n de su navegador.");
        }
    }
};
// 3.Contacto
var contacto = {
    init: function () {
        // contacto.datepicker();
        // contacto.enviar();
        // contacto.format();
        // contacto.regionesComunas();
    },
    datepicker: function () {
        $("[data-toggle='datepicker']").length > 0 && ($.fn.datepicker.languages["es-CL"] = {
            format: "dd/mm/yyyy",
            days: ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"],
            daysShort: ["Dom", "Lun", "Mar", "Mie", "Jue", "Vie", "Sab"],
            daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa"],
            weekStart: 1,
            months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
            monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
        }, $('[data-toggle="datepicker"]').datepicker({
            language: "es-CL",
            startDate: "01/01/1900",
            endDate: "01/01/2001",
            date: new Date("01/01/1980"),
            yearFirst: !0,
            startView: 2
        }));
    },
    enviar: function () {
        $('form').submit(function (e) {
            e.preventDefault();
            var rut = $('#run').val();
            if ($.validateRut(rut)) {
                $('form').trigger("reset");
                swal({
                    type: 'success',
                    title: '¡Gracias!',
                    text: 'Pronto te contactaremos.',
                })
            } else {
                swal({
                    type: 'warning',
                    html: 'El RUT <b> ' + rut + '</b> no es válido.',
                    showCloseButton: true,
                    confirmButtonText: 'Volver'
                });
            }
        });
    },
    format: function () {
        // Only numbers
        $('#run').keyup(function () {
            var val = $(this).val();
            if (isNaN(val)) {
                val = val.replace(/[^\d-.kK]/g, '');
            }
            $(this).val(val);
        });
        $("#run").rut({
            formatOn: 'keyup'
        });
        // Only letters
        $('#nombre').keyup(function () {
            var val = $(this).val();
            if (val) {
                val = val.replace(/[^a-z]/g, '');
            }
            $(this).val(val);
        });
    },
    regionesComunas: function () {
        var RegionesYcomunas = {
            "regiones": [{
                "NombreRegion": "Arica y Parinacota",
                "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
            }, {
                "NombreRegion": "Tarapacá",
                "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
            }, {
                "NombreRegion": "Antofagasta",
                "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
            }, {
                "NombreRegion": "Atacama",
                "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
            }, {
                "NombreRegion": "Coquimbo",
                "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
            }, {
                "NombreRegion": "Valparaíso",
                "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
            }, {
                "NombreRegion": "Región del Libertador Gral. Bernardo O’Higgins",
                "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
            }, {
                "NombreRegion": "Región del Maule",
                "comunas": ["Talca", "ConsVtución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
            }, {
                "NombreRegion": "Región del Biobío",
                "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
            }, {
                "NombreRegion": "Región de la Araucanía",
                "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria", ]
            }, {
                "NombreRegion": "Región de Los Ríos",
                "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
            }, {
                "NombreRegion": "Región de Los Lagos",
                "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
            }, {
                "NombreRegion": "Región Aisén del Gral. Carlos Ibáñez del Campo",
                "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
            }, {
                "NombreRegion": "Región de Magallanes y de la AntárVca Chilena",
                "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "AntárVca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
            }, {
                "NombreRegion": "Región Metropolitana de Santiago",
                "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "TilVl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
            }]
        }
        $(document).ready(function () {
            var iRegion = 0;
            var htmlRegion = '<option value="sin-region" disabled selected >Seleccione región</option>';
            var htmlComunas = '<option value="sin-region" disabled selected >Seleccione comuna</option>';
            $.each(RegionesYcomunas.regiones, function () {
                htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].NombreRegion + '">' + RegionesYcomunas.regiones[iRegion].NombreRegion + '</option>';
                iRegion++;
            });
            $('#regiones').html(htmlRegion);
            $('#comunas').html(htmlComunas);
            $('#regiones').change(function () {
                var iRegiones = 0;
                var valorRegion = $(this).val();
                var htmlComuna = '<option value="sin-comuna">Seleccione comuna</option><option value="sin-comuna">--</option>';
                $.each(RegionesYcomunas.regiones, function () {
                    if (RegionesYcomunas.regiones[iRegiones].NombreRegion == valorRegion) {
                        var iComunas = 0;
                        $.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
                            htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '">' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '</option>';
                            iComunas++;
                        });
                    }
                    iRegiones++;
                });
                $('#comunas').html(htmlComuna);
            });
            $('#comunas').change(function () {
                if ($(this).val() == 'sin-region') {
                    alert('selecciones Región');
                } else if ($(this).val() == 'sin-comuna') {
                    alert('selecciones Comuna');
                }
            });
            $('#regiones').change(function () {
                if ($(this).val() == 'sin-region') {
                    alert('selecciones Región');
                }
            });
        });
    }
};

var dashboard = {
    init: function () {
        dashboard.getIndicadores();
    },
    getIndicadores: function () {
        $.get('https://mindicador.cl/api', function (indicadores) {
            $('.js-indicadores').empty();
            $('.js-indicadores').append('<li class="mt-2">Valor UF: <strong>$' + indicadores.uf.valor + '</span></li>');
            $('.js-indicadores').append('<li class="mt-2">Valor Dólar: <strong>$' + indicadores.dolar.valor + '</strong></li>');
            $('.js-indicadores').append('<li class="mt-2">Valor Euro: <strong>$' + indicadores.euro.valor + '</strong></li>');
            $('.js-indicadores').append('<li class="mt-2">Valor IPC: <strong>%' + indicadores.ipc.valor + '</strong></li>');
            $('.js-indicadores').append('<li class="mt-2">Valor UTM: <strong>$' + indicadores.utm.valor + '</strong></li>');

        }).fail(function () {
            console.log('Error al consumir la API!');
        });
    }
}

var lista = {
    init: function () {
        lista.getProductos();
        lista.getTiendas();
        lista.refreshProductos();
        lista.addProductos();
    },
    getProductos: function () {
        $('.table.productos tbody').empty();
        idLista = $('.js-id-lista').text();

        $.get('/api/v1/productos/?lista__id=' + idLista, function (productos) {
            productosComprados = [];
            cantidadProductosComprados = productosComprados.length;
            cantidadProductos = productos.objects.length;
            totalPresupuestado = 0;
            totalReal = 0;
            esComprado = '';

            productos.objects.forEach(producto => {
                totalPresupuestado = totalPresupuestado + producto.costo_presupuestado;
                totalReal = totalReal + producto.costo_real;
                if (producto.comprado == 1) {
                    productosComprados.push(producto);
                }
                cantidadProductosComprados = productosComprados.length;

                if (producto.comprado == 1) {
                    esComprado = 'Comprado';
                } else {
                    esComprado = '<button class="btn btn-sm btn-info js-update-product" data-product="' + producto.id + '">¡Lo compré!</td>';
                }

                filaProducto = '<tr class="' + esComprado + '"><td>' + producto.nombre + '</td><td>' + producto.tienda.nombre + '</td><td>' + producto.notas_adicionales + '</td><td>' + esComprado +
                    '</td><td><button class="btn btn-sm btn-danger js-remove-product" data-product="' + producto.id + '">Eliminar</button></td></tr>';
                $('.table.productos tbody').append(filaProducto);
            });

            $('.js-cant-productos').text(cantidadProductos);

            $('.js-cant-productos-comprados').text(cantidadProductosComprados = productosComprados.length);

            $('.js-total-presupuestado').text(totalPresupuestado);

            $('.js-total-real').text(totalReal);

            lista.removeProductos();
            lista.updateProductos();
        });

    },
    getTiendas: function () {
        $.get('/api/v1/tiendas/?disponible=1', function (tiendas) {

            tiendas.objects.forEach(tienda => {
                $('.js-tiendas-select').append('<option value="' + tienda.id + '">' + tienda.nombre + '</option>');
            });

        });

    },
    refreshProductos: function () {
        $('.js-reload-productos').click(function () {
            lista.getProductos();
        })
    },
    addProductos: function () {
        $('.js-add-producto').click(function () {
            nombre = $('.js-add-producto-nombre').val();
            costo_real = $('.js-add-producto-costo-real').val();
            costo_presupuestado = $('.js-add-producto-costo-presupuestado').val();
            idLista = $('.js-id-lista').text();
            idTienda = $('.js-tiendas-select option:selected').val();
            notas_adicionales = $('.js-add-producto-notas').val();

            all_filled = nombre.length + costo_real.length + costo_presupuestado;

            if (all_filled == 0) {
                Swal(
                    'Error',
                    'Todos los campos son obligatorios. El producto no ha sido agregado.',
                    'error'
                );
            } else {
                producto = {
                    "nombre": nombre,
                    "costo_presupuestado": costo_presupuestado,
                    "costo_real": costo_real,
                    "tienda": {
                        "id": idTienda
                    },
                    "notas_adicionales": notas_adicionales,
                    "lista": {
                        "id": idLista
                    }
                };
                producto = JSON.stringify(producto);
                $.ajax({
                    // Substitute in your API endpoint here.
                    url: '/api/v1/productos/',
                    contentType: 'application/json',
                    dataType: "application/json",
                    data: producto,
                    method: 'POST',
                    beforeSend: function (jqXHR, settings) {
                        jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                    },
                    complete: function (jqXHR) {
                        console.log(jqXHR);
                        // Your processing of the data here.
                        $('#agregarProductoModal').modal('hide');
                        Swal(
                            'Producto agregado',
                            'El producto ha sido agregado correctamente.',
                            'success'
                        );
                        lista.getProductos();
                    },
                });
            };


        });
    },
    updateProductos: function () {
        $('.js-update-product').click(function (event) {
            product_id = event.target.dataset.product;
            producto = {
                "comprado": 1
            };

            producto = JSON.stringify(producto);

            $.ajax({
                // Substitute in your API endpoint here.
                url: '/api/v1/productos/' + product_id + '/',
                contentType: 'application/json',
                dataType: "application/json",
                data: producto,
                method: 'PUT',
                beforeSend: function (jqXHR, settings) {
                    jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                },
                complete: function (jqXHR) {
                    console.log(jqXHR);
                    // Your processing of the data here.
                    Swal(
                        '¡Producto comprado!',
                        'El producto ha sido actualizado correctamente.',
                        'success'
                    );
                    lista.getProductos();
                },
            });
        });
    },
    removeProductos: function () {
        $('.js-remove-product').click(function (event) {
            product = event.target.dataset.product;
            $.ajax({
                // Substitute in your API endpoint here.
                url: '/api/v1/productos/' + product + '/',
                contentType: 'application/json',
                dataType: "application/json",
                method: 'DELETE',
                beforeSend: function (jqXHR, settings) {
                    jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                },
                complete: function (jqXHR) {
                    console.log(jqXHR);
                    // Your processing of the data here.
                    Swal(
                        'Producto eliminado',
                        'El producto ha sido eliminado correctamente.',
                        'success'
                    );
                    lista.getProductos();
                },
            });
        });
    }
};

var tiendas = {
    init: function () {
        tiendas.aprobar();
    },
    aprobar: function () {
        $('.js-tienda-aprobar').click(function (event) {
            tienda_id = event.target.dataset.tienda;
            tienda = {
                "disponible": 1
            };

            tienda = JSON.stringify(tienda);

            $.ajax({
                // Substitute in your API endpoint here.
                url: '/api/v1/tiendas/' + tienda_id + '/',
                contentType: 'application/json',
                dataType: "application/json",
                data: tienda,
                method: 'PUT',
                beforeSend: function (jqXHR, settings) {
                    jqXHR.setRequestHeader('X-CSRFToken', $('input[name=csrfmiddlewaretoken]').val());
                },
                complete: function (jqXHR) {
                    console.log(jqXHR);
                    // Your processing of the data here.
                    Swal(
                        '¡Tienda aprobada!',
                        'La tienda ha sido actualizada correctamente.',
                        'success'
                    ).then(function () {
                        $('button[data-tienda = ' + tienda_id + ']').addClass('d-none');
                    });
                },
            });
        });
    },
}